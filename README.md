Webview and JavaScript Reflection
====================================
This is a proof of concept example on how enabling JavaScript in Webview and allowing JavaScript to invoke methods in the app through [JavaScriptInterface](http://developer.android.com/reference/android/webkit/WebView.html#addJavascriptInterface\(java.lang.Object, java.lang.String\)) could potentially lead to serious security vulnerabilities in your app.

This is a very serious security problem because it enables JavaScript to use Reflection and find the private and public methods of your app including the inherited methods like Class.forName("java.lang.Runtime"). See [this file](https://bitbucket.org/kalharbi/webviewjscriptreflection/src/49780b0f4db50f58d01f61695bb5cccb09cecdb1/assets/sdcard.html?at=master) for more details.

The good news is that this issue has been fixed in Android 4.2 (API 17) and above as reported [here](http://android-developers.blogspot.com/2013/02/security-enhancements-in-jelly-bean.html#javascript-access).


## Screenshot

![webviewJScriptReflection](https://bytebucket.org/kalharbi/webviewjscriptreflection/raw/d91fbceaae16a1b9d4c031ca0ec163fcf2832e74/screenshots/webviewjscriptreflection.png "webviewJScriptReflection")







