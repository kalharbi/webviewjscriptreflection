package com.kalharbi.mobilesecurity.webviewJScriptReflection;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.webkit.WebView;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Uri uri = Uri.parse("file:///android_asset/sdcard.html");
		WebView myWebView = (WebView) findViewById(R.id.myWebView);
		// Enable JavaScript
		myWebView.getSettings().setJavaScriptEnabled(true);
		// Inject Java Object into the JavaScript context of the webview
		myWebView.addJavascriptInterface(new JsObject(), "jsInject");
		// load the URL into the webview
		myWebView.loadUrl(uri.toString());
	}
}

class JsObject {
	
}
